default:
  retry: 2

stages:
  - test
  - apply

variables:
  ## To implement by users
  # PROJECT_NAME: project-name
  # TF_PROJECT_NAME: tf-folder-name   # Must match TF folder name
  # VAULT_ADDR:                       # Your Vault address

  ## To override if needed
  DISABLE_GITLAB_STATE_CHECK: 0                   # Set to 1 to disable check and warning in safe conditions (otherwise read the warning)
  STATE_PROJECT_ID: $CI_PROJECT_ID                # Users should use separate project for state storage if they don't want devs to read prod state
  TF_APPLY_MANUAL: 1                              # Set to 0 to auto-apply
  TF_STATE_NAME: ${ENV_NAME}_${TF_PROJECT_NAME}   # Can be overridden if needed
  VAULT_NAMESPACE: default                        # Default namespace on open-source Hashicorp Vault
  VAULT_AUTH_PATH: gitlab                         # Uses JWT auth for gitlab mounted at "gitlab" (see tf-vault-setup-manual)
  VAULT_TRANSIT_TFPLAN_KEY: tfplan                # Uses tfplan transit key to encrypt/decrypt plan file  (see tf-vault-setup-manual)
  VAULT_PROJECT: $PROJECT_NAME                    # Matches setup done in tf-vault-setup-manual

  ##
  TF_INIT_FLAGS: -lockfile=readonly
  TF_IN_AUTOMATION: 1

cache:
  key: $CI_COMMIT_REF_SLUG
  paths:
    - ${TF_PROJECT_NAME}/.terraform/providers

.ctx:prod:
  variables:
    ENV_NAME: prod
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

.ctx:prod:apply:
  extends: .ctx:prod
  rules:
    - if: $TF_APPLY_MANUAL == "1" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
    - if: $TF_APPLY_MANUAL == "0" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: on_success

.ctx:review:
  variables:
    ENV_NAME: review
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH

.common:scripts:
  prepare: []
  tf_gitlab_backend:
    - |
      # Export TF backend vars
      export TF_HTTP_PASSWORD=${TF_HTTP_PASSWORD:-"$CI_JOB_TOKEN"}
      export TF_HTTP_USERNAME=${TF_HTTP_USERNAME:-"gitlab-ci-token"}
      export TF_HTTP_ADDRESS=${CI_API_V4_URL}/projects/${STATE_PROJECT_ID}/terraform/state/${TF_STATE_NAME}
      export TF_HTTP_LOCK_ADDRESS=${TF_HTTP_ADDRESS}/lock
      export TF_HTTP_LOCK_METHOD=POST
      export TF_HTTP_UNLOCK_ADDRESS=${TF_HTTP_LOCK_ADDRESS}
      export TF_HTTP_UNLOCK_METHOD=DELETE
      export TF_HTTP_RETRY_WAIT_MIN=5
  tf_init:
    - cd $TF_PROJECT_NAME
    - terraform --version
    - terraform init
  cleanup: []
  vault_login:
    - |
      # Login to Vault if needed
      # TODO errors in subshell (vault write ...) do not bubble up
      if test -n "$VAULT_ADDR" && test -n "$VAULT_AUTH_PATH" && test -n "$VAULT_PROJECT" && test -n "$ENV_NAME"; then
        echo "Vault: Authenticate using JWT at auth/${VAULT_AUTH_PATH}/login with role ${VAULT_PROJECT}_${ENV_NAME}"
        export VAULT_TOKEN="$(vault write -field=token auth/${VAULT_AUTH_PATH}/login role=${VAULT_PROJECT}_${ENV_NAME} jwt=${CI_JOB_JWT})"
        vault token lookup | egrep 'policies|identity_policies'
      fi
  vault_logout:
    - |
      # Revoke Vault token if applicable
      if test -n "$VAULT_ADDR" && test -n "$VAULT_TOKEN"; then
        echo "Vault: Revoke token"
        vault token revoke -self || true
      fi
  vault_plan_encrypt:
    - |
      # Handle TF plan encryption if user setup for it
      if test -n "$VAULT_ADDR" && test -n "$VAULT_PROJECT" && test -n "$ENV_NAME" && test -n "$VAULT_TRANSIT_TFPLAN_KEY"; then
        echo "Vault: Encrypting plan file (and deleting unencrypted plan file)"
        vault write -field ciphertext transit/encrypt/${VAULT_PROJECT}_${ENV_NAME}_${VAULT_TRANSIT_TFPLAN_KEY} \
          plaintext="$(cat .terraform.plan | base64)" > .terraform.plan.encrypted
        rm .terraform.plan
      fi
  vault_plan_decrypt:
    - |
      # Handle TF plan encryption if user setup for it
      if test -n "$VAULT_ADDR" && test -n "$VAULT_PROJECT" && test -n "$ENV_NAME" && test -n "$VAULT_TRANSIT_TFPLAN_KEY"; then
        echo "Vault: Decrypting plan file"
        vault write -field plaintext transit/decrypt/${VAULT_PROJECT}_${ENV_NAME}_${VAULT_TRANSIT_TFPLAN_KEY} \
          ciphertext="$(cat .terraform.plan.encrypted)" | base64 -d > .terraform.plan
      fi

.plan:
  artifacts:
    expire_in: 1 hour
    paths:
      - ${TF_PROJECT_NAME}/.terraform.plan
      - ${TF_PROJECT_NAME}/.terraform.plan.encrypted
  resource_group: ${ENV_NAME}_${TF_PROJECT_NAME}
  script:
    - !reference [".common:scripts", "vault_login"]
    - !reference [".common:scripts", "prepare"]
    - !reference [".common:scripts", "tf_gitlab_backend"]
    - !reference [".common:scripts", "tf_init"]
    - terraform plan -input=false -out=.terraform.plan
    - !reference [".common:scripts", "vault_plan_encrypt"]
    - !reference [".common:scripts", "cleanup"]
    - !reference [".common:scripts", "vault_logout"]
  stage: test

.apply:
  resource_group: ${ENV_NAME}_${TF_PROJECT_NAME}
  retry: 0
  script:
    - !reference [".common:scripts", "vault_login"]
    - !reference [".common:scripts", "prepare"]
    - !reference [".common:scripts", "tf_gitlab_backend"]
    - !reference [".common:scripts", "tf_init"]
    - !reference [".common:scripts", "vault_plan_decrypt"]
    - terraform apply -input=false -auto-approve .terraform.plan
    - !reference [".common:scripts", "cleanup"]
    - !reference [".common:scripts", "vault_logout"]
  stage: apply

.validate:
  stage: test
  script:
    - !reference [".common:scripts", "vault_login"]
    - !reference [".common:scripts", "prepare"]
    - !reference [".common:scripts", "tf_gitlab_backend"]
    - !reference [".common:scripts", "tf_init"]
    - terraform validate
    - !reference [".common:scripts", "cleanup"]
    - !reference [".common:scripts", "vault_logout"]

prod:validate:
  extends: [".validate", ".ctx:prod"]

review:validate:
  extends: [".validate", ".ctx:review"]

prod:plan:
  extends: [".plan", ".ctx:prod"]

review:plan:
  extends: [".plan", ".ctx:review"]

prod:apply:
  extends: [".apply", ".ctx:prod:apply"]
  dependencies:
    - prod:plan

review:apply:
  extends: [".apply", ".ctx:review"]
  dependencies:
    - review:plan

gitlab:state:check:
  stage: test
  image: alpine
  needs: []
  retry: 0
  script:
    - |-
      # Project ID vs State ID safety check
      if [ "$STATE_PROJECT_ID" != "$CI_PROJECT_ID" ]; then
        # exit early if we can
        echo "Project looks good!"
        exit 0
      fi
      TXT=$(cat << DESCRIPTION
      This project uses GitLab managed Terraform State, and is storing the state
      in the same project as the source code.
      .
      While this can be fine in some situation, in others it introduces a major
      security concern since project members only need the developer role to
      read the state of your protected branches for example.
      .
      So if you are using GitLab in an Organization, and inherit many members,
      see the "Safe usage" section below. Otherwise see "Not an issue for this
      project" to disable this warning and check job.
      .
      See this GitLab issue for some promising ideas to fix this in the future:
      https://gitlab.com/gitlab-org/gitlab/-/issues/227108
      .
      ## Not an issue for this project
      .
      Here is how to disable the warning check job.
      .
      Set this variable in your gitlab-ci:
      "DISABLE_GITLAB_STATE_CHECK: 1"
      .
      ## Safe usage
      .
      Here is how to fix this:
      - Create a new GitLab group at the top level, dedicated for X project
        sensitive things like TF states.
      - Within that new group, create a "tfstate" project (or multiple if needed)
      - In that new project, create a new token with API scope and maintainer role.
      - Capture this new project ID, that will be used to push TF states to.
      - Store that token in secret management ideally, that a pipeline can pull
        from the original source code repo.
      - Set "STATE_PROJECT_ID: <tfstate project ID>" in your gitlab-ci file
      DESCRIPTION
      )
      echo "$TXT"
      # Fail job
      exit 1
  rules:
    - if: $DISABLE_GITLAB_STATE_CHECK == "1"
      when: never
    - when: always
      allow_failure: true
