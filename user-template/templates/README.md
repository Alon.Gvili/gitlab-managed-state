# TF Templates

Here is a list of templates ready to use or with a handful of variables to set.

For all of them, simply copy or move them into your project root folder.

## tf-test

Simple empty Terraform module for dev, review and prod style environments.

To start it up in dev within devcontainer, after copying template in your root project folder:

```bash
make check.vars    # and follow those base instructions if not done already
cd tf-test-vault
make tf-init tf-validate tf-apply
```

Otherwise to make the gitlab-ci version work:

- Nothing to do for this one. Simply push as is.
- Note: as you will likely notice in your pipelines, there can easily be insecure situations
  with GitLab Managed Terraform states. For instance, any developers and above can read your
  state files and the sensitive information in it. See pipeline `check` job for more info.

## tf-test-vault

Simple module that gets fed sensitive values through environment variables and Vault.

This leverages the Vault setup done by the `tf-vault-set-manual` module, which should
be used first before this one.

To start it up in dev within devcontainer, after copying template in your root project folder:

```bash
make check.vars    # and follow those base instructions if not done already
cd tf-test-vault
make tf-init tf-validate tf-apply
```

Otherwise to make the gitlab-ci version work:

- In either your root `.gitlab-ci.yml` or the one within the `tf-test-vault`, uncomment and
  set the right `VAULT_ADDR`. Same for `VAULT_NAMESPACE` if using Vault enterprise.

## tf-vault-setup-manual

A Terraform module meant to be run manually and locally by a maintainer, leveraging
the devcontainer included in this project template, to get up and running on any
computer running docker and able to mount local folders with the exact environment
and tools needed to apply this module.

What this module does is configure all the parts needed in a Vault instance, for the
`tf-test-vault` pipeline needs, including plan encryption/decryption, JWT authentication,
for prod, review and dev context. It uses a project name and environment name in all Vault
resources it creates.

This is NOT intended to run in a pipeline.

To start it up within devcontainer, after copying template in your root project folder:

```bash
make check.vars    # and follow those base instructions if not done already
cd tf-vault-setup-manual
make tf-init tf-validate tf-apply
```
