#!make

# This file holds targets to help with template upgrades.

.PHONY: template.diff
template.diff:
	@$(MAKE) template.diff.main
	@$(MAKE) template.diff.tmpl.tf-test
	@$(MAKE) template.diff.tmpl.tf-test-vault
	@$(MAKE) template.diff.tmpl.tf-vault-setup-manual

.PHONY: .tmpl.diff.base
.tmpl.diff.base:
	@test -n "${SOURCE}" && test -d ${SOURCE} || { echo "SOURCE must be set and be a directory"; exit 1; }
	@diff --normal --color --recursive \
		--exclude=local.env --exclude=ref --exclude=.git \
		--exclude=.terraform --exclude=.terraform.lock.hcl \
		${SOURCE}/ ${DC_TEMPLATE}/${TARGET} || true

.PHONY: template.diff.main
template.diff.main: SOURCE=.
template.diff.main: TARGET=
template.diff.main: .tmpl.diff.base

.PHONY: .tmpl.diff.base.template
.tmpl.diff.base.template: SOURCE=${TEMPLATE}
.tmpl.diff.base.template: TARGET=templates/${TEMPLATE}
.tmpl.diff.base.template: .tmpl.diff.base

.PHONY: template.diff.tmpl.tf-test template.diff.tmpl.tf-test-vault template.diff.tmpl.tf-vault-setup-manual
template.diff.tmpl.tf-test: TEMPLATE=tf-test
template.diff.tmpl.tf-test: .tmpl.diff.base.template
template.diff.tmpl.tf-test-vault: TEMPLATE=tf-test-vault
template.diff.tmpl.tf-test-vault: .tmpl.diff.base.template
template.diff.tmpl.tf-vault-setup-manual: TEMPLATE=tf-vault-setup-manual
template.diff.tmpl.tf-vault-setup-manual: .tmpl.diff.base.template

.PHONY: template.tag-upgrade
template.tag-upgrade:
	@test -n "${NEXT_TAG}" || { echo "NEXT_TAG must bet set"; exit 1; }
	@TARGET=.devcontainer/Dockerfile $(MAKE) .template.tag-upgrade
	@TARGET=tf-test/.gitlab-ci.yml $(MAKE) .template.tag-upgrade
	@TARGET=tf-test-vault/.gitlab-ci.yml $(MAKE) .template.tag-upgrade

.PHONY: .template.tag-upgrade
.template.tag-upgrade:
	@test -n "${NEXT_TAG}" || { echo "NEXT_TAG must be set"; exit 1; }
	@test -e "${TARGET}" \
		&& sed -i -e "s|:${DC_TAG}|:${NEXT_TAG}|g" -e "s|/${DC_TAG}|/${NEXT_TAG}|g" ${TARGET} \
		|| echo "Could not find target ${TARGET} . Skipping."
