#make

.PHONY: vault-login
vault-login:
	@echo "[] Login to Cifra (Vault) using"
	VAULT_NAMESPACE="" vault login -method=oidc

.PHONY: .vault-check
vault-check:
	@vault token lookup 1>/dev/null 2>/dev/null || { echo "Not logged into Vault"; false; }

.PHONY: vault-token-lookup
vault-token-lookup:
	vault token lookup
