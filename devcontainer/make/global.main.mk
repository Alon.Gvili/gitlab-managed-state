#!make

# hide entering/leaving directory messages
MAKEFLAGS += --no-print-directory

all: info

include ${DC_LIB_MAKE}/check.mk
include ${DC_LIB_MAKE}/global.info.mk
include ${DC_LIB_MAKE}/template.mk
include ${DC_LIB_MAKE}/vault.mk
